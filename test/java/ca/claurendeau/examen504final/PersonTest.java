package ca.claurendeau.examen504final;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Feelings.Feelings;
import Mood.HappyMood;
import Mood.SadMood;
import Mood.UnhappyMood;
import Person.Person;

public class PersonTest {
	
	Person personneHeureuse;
	Person personneMalheureuse;
	Person personneTriste;
	
	@Before
	public void setUp(){
		personneHeureuse = new Person(new HappyMood());
		personneMalheureuse = new Person(new UnhappyMood());
		personneTriste = new Person(new SadMood());
	}
	
	@After
	public void tearDown(){
		personneHeureuse = null;
		personneMalheureuse = null;
		personneTriste = null;
	}

    @Test
    public void testPersonne() {
        Assert.assertEquals(personneHeureuse.getMood().toString(), "heureuse");
        Assert.assertEquals(personneMalheureuse.getMood().toString(), "malheureuse");
        Assert.assertEquals(personneTriste.getMood().toString(), "triste");
    }
    
    @Test
    public void testExpressFeelings(){
    	Assert.assertEquals(Feelings.Message.HAPPY.toString(), personneHeureuse.expressFeelings());
    	Assert.assertEquals(Feelings.Message.UNHAPPY.toString(), personneMalheureuse.expressFeelings());
    	Assert.assertEquals(Feelings.Message.SAD.toString(), personneTriste.expressFeelings());
    }
}
