package Mood;

import Feelings.Feelings;
import Feelings.Feelings.Message;

public class SadMood implements Mood{

	@Override
	public String expressFeelings() {
		return Feelings.Message.SAD.toString();
	}
	
	@Override
	public String toString(){
		return "triste";
	}
}
