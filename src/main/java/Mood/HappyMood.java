package Mood;

import Feelings.Feelings;
import Feelings.Feelings.Message;

public class HappyMood implements Mood{

	@Override
	public String expressFeelings() {
		return Feelings.Message.HAPPY.toString();
	}
	
	@Override
	public String toString(){
		return "heureuse";
	}
}
