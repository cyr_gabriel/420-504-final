package Mood;

import Feelings.Feelings;
import Feelings.Feelings.Message;

public class UnhappyMood implements Mood{
	
	@Override
	public String expressFeelings() {
		return Feelings.Message.UNHAPPY.toString();
	}
	
	@Override
	public String toString(){
		return "malheureuse";
	}

}
