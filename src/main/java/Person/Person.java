package Person;

import Mood.Mood;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public class Person {
    
    private Mood mood;

	public Person(Mood mood) {
        this.mood = mood;
    }

	public Mood getMood() {
		return mood;
	}

	public void setMood(Mood mood) {
		this.mood = mood;
	}

	public String expressFeelings() {
		return this.mood.expressFeelings();
	}    
	
	@Override
    public String toString() {
        return "Personne [humeur=" + mood.toString() + "]";
    }
}
