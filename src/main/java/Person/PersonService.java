package Person;

import java.util.ArrayList;
import java.util.List;

import Mood.HappyMood;
import Mood.SadMood;
import Mood.UnhappyMood;

public class PersonService {
    
    public static void main(String[] args) {
        
        List<Person> personnes = new ArrayList<>();
        personnes.add(new Person(new HappyMood()));
        personnes.add(new Person(new UnhappyMood()));
        personnes.add(new Person(new SadMood()));
        
        personnes.stream()
                 .forEach(System.out::println);
        
        for (Person personne : personnes) {
            System.out.println(personne.expressFeelings());
        }
    }
}
