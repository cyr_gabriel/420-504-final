package Feelings;

public class Feelings {
	
	public enum Message{
		HAPPY{
			public String toString(){
				return "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!";
			}
		}, 
		UNHAPPY{
			public String toString(){
				return "J'ai besoin d'un MacBook Pro pour être une personne heureuse!";
			}
		}, 
		SAD{
			public String toString(){
				return "Je fais parti des gens qui n'auront jamais de MacBook Pro";
			}
		}
	}

}
